<?php
	if(!empty($_SERVER['HTTP_CLIENT_IP'])){//правльное определение IP адреса
		$ip=$_SERVER['HTTP_CLIENT_IP'];
	}elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
		$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	}else{
		$ip=$_SERVER['REMOTE_ADDR'];
	}


	$array_ip_allowed=['33.23.123.213','53.54.75.121','5.43.54.66'];//... Список разрпешенных IP адресов

	if(filter_var($ip,FILTER_VALIDATE_IP)){//Проверка ip на валидность, если валиден - работаем с ним
		foreach($array_ip_allowed as$value){
			if($ip==$value){
				$allowed_ip=$value;//Записываем ip в разрешенный ip
				break;
			}
		}

		if(!isset($allowed_ip)){//Если есть переменная с разрешенным ip - не будет определять геолокацию
			$query=@unserialize(file_get_contents('http://ip-api.com/php/'.$ip.'?lang=ru'));//lang=ru - язик ответа сервера.
			if($query&&$query['status']=='success'){
				if($query['country']=="Польша")exit('2');//Посетитель из Польши - завершаем скрипт
			}else{
				exit('1');//Сервис отказался работать - завершаем скрипт
			}
		}
	}